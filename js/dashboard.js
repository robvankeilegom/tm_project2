$(function() {
    $( "#vervoer" ).buttonset();
});
$(function() {
    $( "#openbaarvervoer" ).buttonset();
});

$( document ).ready(function() {
    var autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('address_text')),
        { types: ['geocode'] });
    
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        var id = place.place_id;
        document.getElementById('placeid').value = id;
    });
});
