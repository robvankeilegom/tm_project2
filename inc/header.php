<!-- JS -->
<script src="js/jquery.js"></script> <!--  DEFAULT JQUERY FILE -->
<script src="js/jquery-ui.js"></script> <!--  DEFAULT JQUERY FILE -->
<script src='https://www.google.com/recaptcha/api.js' async defer></script> <!-- RECAPTCHA -->

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/fullcalendar.css" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/onepage-scroll.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="css/main.css" />

<!-- FONT -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css' />

<!-- SESSION -->
<?php
    session_start();

    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
        // last request was more than 30 minutes ago
        session_unset();     // unset $_SESSION variable for the run-time 
        session_destroy();   // destroy session data in storage
    }

    $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
?>

<!-- PHP INCLUDES -->
<?php
    include ("inc/connection.php");
?>

<!-- GOOGLE API KEY -->
<?php
    $google_api_key = "AIzaSyANUu89aev6NFYdrn48Se1pmrw3_cwIW6A";
?>