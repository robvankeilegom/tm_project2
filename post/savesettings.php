<?php

    session_start();
    require('../inc/connection.php');
    $auto = 0;
    $openbaar = 0;
    $trein = 0;
    $tram = 0;
    $bus = 0;
    $address_text = "";
    $placeid = "";


    if (isset ($_POST["vervoer"])) {
        if ($_POST["vervoer"] == "car") {
            $auto = 1;
        } else {
            $openbaar = 1;  
        }
    }
    if (isset ($_POST["trein"])) {
        $trein = 1;
    }
    if (isset ($_POST["tram"])) {
        $tram = 1;
    }
    if (isset ($_POST["bus"])) {
        $bus = 1;
    }
    if (isset ($_POST["address_text"])) {
        $address_text = mysqli_real_escape_string($conn,htmlspecialchars($_POST["address_text"]));
    }
    if (isset ($_POST["placeid"])) {
        $placeid = $_POST["placeid"];
    }


    $query = "";
    $query .= "UPDATE `t_settings` SET "; 
    $query .= "`car` = " . $auto . ", ";
    $query .= "`transit` = " . $openbaar . ", ";
    $query .= "`train` = " . $trein . ", ";
    $query .= "`tram` = " . $tram . ", ";
    $query .= "`bus` = " . $bus . ", ";
    $query .= "`address_text` = '" . $address_text . "', ";
    $query .= "`address_google_id` = '" . $placeid . "' ";
    $query .= "WHERE `user_id` = " . $_SESSION['ID'];

    $result = mysqli_query($conn, $query);

   
    header('Location: ../dashboard.php');    
   

       
?>