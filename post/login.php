<?php

session_start();

require('../inc/connection.php');

$email = mysqli_real_escape_string($conn,htmlspecialchars($_POST["email"]));

$query = "SELECT `id`, `name`, `password` FROM `t_users` WHERE lower(`email`) = lower('$email')";

$result = mysqli_query($conn, $query);


if ($result->num_rows > 0) {

    $row = $result->fetch_assoc();
    if (password_verify($_POST["password"], $row["password"])) { 
        $_SESSION['ID'] = $row["id"];
        $_SESSION['NAME'] = $row["name"];
        $_SESSION['IP'] = $_SERVER['REMOTE_ADDR'];
        header('Location: ../dashboard.php');
        exit();
    }
}

header('Location: ../index.php?failed=1');

exit();
?>