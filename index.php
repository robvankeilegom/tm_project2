<html>
<head>
    <?php 
        include ("inc/header.php");
    ?>

<link rel="stylesheet" type="text/css" href="css/index.css">
<script src="js/jquery.onepage-scroll.js"></script> 
<script src="js/index.js"></script> 
<title>Home</title>    
</head>
<body>
<nav>
    <a onclick='$("#main-wrapper").moveTo(1);'>Registreren</a>|
    <a onclick='$("#main-wrapper").moveTo(2);'>Info</a>|
    <a onclick='$("#main-wrapper").moveTo(3);'>Contact</a>|
    <a onclick='$("#main-wrapper").moveTo(4);'>FAQ</a>|
    <?php
        if (isset($_SESSION['ID'])) {
            echo ("<a href=\"dashboard.php\">Log In</a>"); 
        } else {
    
            echo ("<a onclick='$(\"#main-wrapper\").moveTo(5);'>Log In</a>");
        }
    ?>
</nav> 
   
<div id="main-wrapper">
<section id="welcome">
    <h1>Welkom</h1>
    <div class="ui-widget form-error hide" id="errormsg">
	<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;" id="form-error">
        <p id="errormsgbody"></p>
		
	</div>
    </div>
<!--
KEYS FOR CAPTCHA TESTING PURPOSE
Site key: 6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI
-->
    
    <form id="frmregister" method="post">
        <input type="email" id="email" name="email" placeholder="E-mail" class="defaulttextbox"/>
        <div class="g-recaptcha" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
        <input type="submit" id="submitbtn"  value="Registreren" onclick="return fullregisterform()" />
        <p class="note">Al een account? <a onclick='$("#main-wrapper").moveTo(5);'>Log In</a>.</p>
    </form>
</section>
<section id="about">
    <h1>Info</h1>
    <div class="content justify">
        <p>Altijdoptijd.be is een website gemaakt om het leven van de student gemakkeijker te maken. De website bundelt je uurrooster en de uren van het openbaar vervoer op 1 pagina. Hierdoor kan je gemakkelijk en in 1 oogopslag vinden wanneer je je wekker moet zetten! </p>
        <p>Deze website is gemaakt als project door Marlon Stoops en Rob Van Keilegom. Wij zijn 2 studenten aan Thomas More: Campus De Nayer.</p>
    </div>
</section>
<section id="contact">
    <h1>Contact</h1>
        <div class="content center">
        <p>
            Voor vragen of opmerkingen kan je ons altijd contacteren via email. <br />
            RobVanKeilegom@hotmail.com <br />
            Marlon.Stoops@hotmail.com
        </p>
    </div>
</section>
<section id="faq">
    <h1>Frequently Asked Questions</h1>
    <div class="content center">
        <p class="question">Q: Is de website gratis?</p>
            <p class="answer">A: De website is volledig gratis, en blijft dat ook!</p>
            <p class="question">Q: Waar halen jullie de uren van het openbaar vervoer? </p>
            <p class="answer">A: Deze worden via da API van google opgehaald.</p>
    </div>
</section>
<section id="login">
    <h1>Log In</h1>
    <?php
    if (isset($_SESSION['ID'])) {
        echo ("Je bent al ingelogd, klik <a href=\"dashboard.php\">hier</a> om naar je profiel te gaan."); 
    } else {
        echo ('<form method="post" action="post/login.php">');
        echo ('<input type="email" name="email" placeholder="E-mail" class="defaulttextbox"/>');
        echo ('<input type="password" name="password" placeholder="Wachtwoord" class="defaulttextbox"/>');
        echo ('<input type="submit" value="Inloggen" />');
        echo ('</form>');
    }
    ?>

    </form>
</section>
</div>
</body>
</html>